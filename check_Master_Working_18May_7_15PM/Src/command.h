/************ Command Struture *******************/
#define PWMInit                              0x01
#define PWMDef                               0x03
#define PWMStart                             0x05
#define PWMStop                              0x00

/************ PWM Channels ***********************/
#define PWM0                                0x0001
#define PWM1                                0x0002
#define PWM2                                0x0004
#define PWM3                                0x0008
#define PWM4                                0x0010
#define PWM5                                0x0020
#define PWM6                                0x0040
#define PWM7                                0x0080
#define PWM8                                0x0100
#define PWM9                                0x0200
#define PWM10                               0x0400
#define PWM11                               0x0800
#define PWM12                               0x1000
#define PWM13                               0x2000
#define PWM14                               0x4000
#define PWM15                               0x8000

/************ Timer Frequency ***********************/
#define TimerFrequency_MHz 84
