
extern void PWM_Init(u8 InitCmd);
extern void PWM_Def(u8 DefCmd);
extern void PWM_Start(u8 StartCmd);
extern void PWM_Stop(u8 StopCmd);

extern void PWM_Def_Pattern(u16 PWM_SigVal, u32 PWM_TimInst1, u32 PWM_TimInst2);
extern void sorting_PWM_Def(void);
extern void make_PWMPattern_Time_Arr(void);

extern volatile u8 i;
extern u8 Cmd_Type ;

extern u16 InitPattern;

extern u32 PWMPattern_Time[14][2];

extern u32 No_Of_Repetition;
extern u32 TimePeriod ;
