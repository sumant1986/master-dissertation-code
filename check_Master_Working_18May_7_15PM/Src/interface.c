#include "stm32f4xx.h"
#include "command.h"
#include "function_api.h"

static u8 Init_Cmd_Send_Status = 0;
static u8 Init_Pattern_Send_Status = 0;

static u8 Def_Cmd_Send_Status = 0;
static u8 Def_Pattern_Send_Status = 0;
static u8 Def_TimeInst_Send_Status = 0;

static u8 Start_Cmd_Send_Status = 0;
static u8 Start_Rept_Send_Status = 0;
static u8 FirstFrame_Status = 0;
static u8 SecondFrame_Status = 0;
static u16 Send_Frame = 0;

static u8 Stop_Cmd_Send_Status = 0;

/********************************* Function Declaration *******************/



/********************************** Function Definition *******************/

void PWM_Init(u8 InitCmd)
{
    Cmd_Type = InitCmd;

    SPI_I2S_SendData(SPI2,InitCmd);
    Init_Cmd_Send_Status = 1;
    SPI_Cmd(SPI2,ENABLE);
}

void PWM_Def(u8 DefCmd)
{
    SPI_I2S_SendData(SPI2,DefCmd);
    Def_Cmd_Send_Status = 1;
    SPI_Cmd(SPI2,ENABLE);
}

void PWM_Start(u8 StartCmd)
{
    SPI_I2S_SendData(SPI2,StartCmd);
    Start_Cmd_Send_Status = 1;
    SPI_Cmd(SPI2,ENABLE);
}

void PWM_Stop(u8 StopCmd)
{
    SPI_I2S_SendData(SPI2,StopCmd);
    Stop_Cmd_Send_Status = 1;
    SPI_Cmd(SPI2,ENABLE);

}
void SPI2_IRQHandler(void)
{
    if(SPI_I2S_GetFlagStatus(SPI2, SPI_FLAG_TXE) != RESET)
    {
        switch(Cmd_Type)
        {
        case PWMInit:
            SPI_Cmd(SPI2,DISABLE);

            if(Init_Cmd_Send_Status == 1)
            {
                Init_Cmd_Send_Status = 0;
                Init_Pattern_Send_Status = 1;
                //SPI_TransmitCRC(SPI2);
                SPI_I2S_SendData(SPI2,InitPattern);
                SPI_Cmd(SPI2,ENABLE);

            }
            else if(Init_Pattern_Send_Status == 1)
            {
                Init_Pattern_Send_Status = 0;
            }
            else
            {
                ;
            }


        break;

        case PWMDef:
            SPI_Cmd(SPI2,DISABLE);

            if(Def_Cmd_Send_Status == 1)
            {
                    Def_Cmd_Send_Status = 0;
                    Def_TimeInst_Send_Status = 1;
                    SPI_I2S_SendData(SPI2,(u16)(PWMPattern_Time[i][0] & 0x0000FFFF));
                    SPI_Cmd(SPI2,ENABLE);

            }
            else if(Def_TimeInst_Send_Status == 1)
            {

                    if(FirstFrame_Status == 0)
                    {
                        FirstFrame_Status = 1;
                        Send_Frame = (u16)(PWMPattern_Time[i][1] & 0x0000FFFF);
                        SPI_I2S_SendData(SPI2,Send_Frame);
                        SPI_Cmd(SPI2,ENABLE);

                    }
                    else if(SecondFrame_Status == 0)
                    {
                         FirstFrame_Status = 0;
                         Def_TimeInst_Send_Status = 0;
                         Send_Frame = (u16)((PWMPattern_Time[i][1] & 0xFFFF0000)>>16);
                         SPI_I2S_SendData(SPI2,Send_Frame);
                         SPI_Cmd(SPI2,ENABLE);

                    }
                    else
                    {
                        ;
                    }

            }
            else
            {
                ;
            }
        break;

        case PWMStart:
             SPI_Cmd(SPI2,DISABLE);

             if(Start_Cmd_Send_Status == 1)
             {
                  if(FirstFrame_Status == 0)
                  {
                      FirstFrame_Status = 1;
                      Send_Frame = (u16)(No_Of_Repetition & 0x0000FFFF);
                      SPI_I2S_SendData(SPI2,Send_Frame);
                      SPI_Cmd(SPI2,ENABLE);

                  }
                  else if(SecondFrame_Status == 0)
                  {
                      FirstFrame_Status = 0;
                      Start_Cmd_Send_Status = 0;
                      Start_Rept_Send_Status = 1;
                      Send_Frame = (u16)((No_Of_Repetition & 0xFFFF0000)>>16);
                      SPI_I2S_SendData(SPI2,Send_Frame);
                      SPI_Cmd(SPI2,ENABLE);

                  }
                  else
                  {
                     ;
                  }
             }
             else if(Start_Rept_Send_Status == 1)
             {
                if(FirstFrame_Status == 0)
                {
                  FirstFrame_Status = 1;
                  Send_Frame = (u16)(TimePeriod & 0x0000FFFF);
                  SPI_I2S_SendData(SPI2,Send_Frame);
                  SPI_Cmd(SPI2,ENABLE);

                }
                else if(SecondFrame_Status == 0)
                {
                    FirstFrame_Status = 0;
                    Start_Rept_Send_Status = 0;
                    Send_Frame = (u16)((TimePeriod & 0xFFFF0000)>>16);
                    SPI_I2S_SendData(SPI2,Send_Frame);
                    SPI_Cmd(SPI2,ENABLE);
                }
                else
                {
                    ;
                }

             }
             else
             {
                 ;
             }
        break;

        case PWMStop:
            Stop_Cmd_Send_Status = 0;
            SPI_Cmd(SPI2,DISABLE);
            //PWM_Start(PWMStart);
        break;
        }
    }
}

