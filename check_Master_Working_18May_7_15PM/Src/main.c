#include "main.h"
#include "stm32f4xx_tim.h"
#include "command.h"
#include "function_api_interface.h"

static void GPIO_Pin_Config(uint32_t GPIO_Pin_x,GPIOMode_TypeDef GPIO_Mode_x,GPIO_TypeDef* GPIOx);
static void NVIC_Config(uint8_t x_IRQn,uint8_t Priority_No,uint8_t SubPrio_No,FunctionalState Enable_Or_Disable);
static void SPI_Config(void);
static void Configure_PD0(void);

GPIO_InitTypeDef  GPIO_InitStructure;
NVIC_InitTypeDef nvicStructure;

SPI_InitTypeDef SPI_InitStructure;

GPIO_InitTypeDef GPIO_InitStruct;
EXTI_InitTypeDef EXTI_InitStruct;
NVIC_InitTypeDef NVIC_InitStruct;

static u32 MAX_Size = 0;
static u32 instCount = 0;
static u32 tcount = 0;

/******************************** User Define **********************************/
#define Total_Time_Instances 66 /* How many Time instance we deine in
                                   PWM_Def_Pattern function its value would be
                                   2* No. of calls of PWM_Def_Pattern() function */
#define Continue_Exec 0  /* After PWM Stop do we want to continue again from the
                            PWM start set 1 otherwise set 0 */

#define PWM_Pattern_Define 1 /* Whether we want our own desired pattern or the
                                default one which is initialization pattern */
/*********************************************************************************/

struct PWM_Definition
{
   u32 PWM_Time_Instance_value;
   u16 PWM_Sig_Value;
   u8 ONorOFF ;
} PWM_Define_Par[Total_Time_Instances], temp;




/************************************************************************************
 *  File:     main.c
 *  Purpose:  Cortex-M4 main file.
 *            Replace with your code.
 *  Date:     05 July 2013
 *  Info:     If __NO_SYSTEM_INIT is defined in the Build options,
 *            the startup code will not branch to SystemInit()
 *            and the function can be removed
 ************************************************************************************/


#ifndef __NO_SYSTEM_INIT
//void SystemInit()
//{}
#endif

/*********************************************************************
 *
 *  main()
 *********************************************************************/
void main()
{
  /******************************************************************
   *
   *  Place your code here.
   ******************************************************************/


   /************* PWM Parameter Definition User Decides *************/
   #if PWM_Pattern_Define
   InitPattern = 0x0000;

   PWM_Def_Pattern(PWM0, 2, 3);
   PWM_Def_Pattern(PWM0, 8, 11);

   PWM_Def_Pattern(PWM1, 2, 3);
   PWM_Def_Pattern(PWM1, 6, 7);
   PWM_Def_Pattern(PWM1, 11, 15);

   PWM_Def_Pattern(PWM2, 2, 3);
   PWM_Def_Pattern(PWM2, 10, 13);

   PWM_Def_Pattern(PWM3, 2, 3);
   PWM_Def_Pattern(PWM3, 6, 7);
   PWM_Def_Pattern(PWM3, 8, 14);

   PWM_Def_Pattern(PWM4, 10, 12);

   PWM_Def_Pattern(PWM5, 6, 7);
   PWM_Def_Pattern(PWM5, 9, 12);
   PWM_Def_Pattern(PWM5, 14, 15);

   PWM_Def_Pattern(PWM6, 4, 8);
   PWM_Def_Pattern(PWM6, 11, 15);

   PWM_Def_Pattern(PWM7, 6, 7);
   PWM_Def_Pattern(PWM7, 12, 15);

   PWM_Def_Pattern(PWM8, 8, 11);

   PWM_Def_Pattern(PWM9, 7, 12);

   PWM_Def_Pattern(PWM10, 9, 13);

   PWM_Def_Pattern(PWM11, 7, 8);
   PWM_Def_Pattern(PWM11, 13, 14);

   PWM_Def_Pattern(PWM12, 4, 5);
   PWM_Def_Pattern(PWM12, 9, 14);

   PWM_Def_Pattern(PWM13, 4, 5);
   PWM_Def_Pattern(PWM13, 7, 8);
   PWM_Def_Pattern(PWM13, 10, 13);


   PWM_Def_Pattern(PWM14, 4, 5);
   PWM_Def_Pattern(PWM14, 9, 12);

   PWM_Def_Pattern(PWM15, 4, 5);
   PWM_Def_Pattern(PWM15, 7, 8);
   PWM_Def_Pattern(PWM15, 10, 12);

   No_Of_Repetition =  0x00055555;
   TimePeriod = 17 * TimerFrequency_MHz;


   sorting_PWM_Def();
   make_PWMPattern_Time_Arr();

   #endif
   /******************************************************************/


   /*PWM_Def_Pattern(PWM1, 2, 4);

   PWM_Def_Pattern(PWM2, 3, 5);
   PWM_Def_Pattern(PWM3, 4, 6);
   PWM_Def_Pattern(PWM4, 5, 7);
   PWM_Def_Pattern(PWM5, 6, 8);
   PWM_Def_Pattern(PWM6, 7, 9);
   PWM_Def_Pattern(PWM7, 8, 10);
   PWM_Def_Pattern(PWM0, 2, 11);
   PWM_Def_Pattern(PWM1, 3, 12);
   PWM_Def_Pattern(PWM2, 4, 13);
   PWM_Def_Pattern(PWM3, 5, 14);
   PWM_Def_Pattern(PWM4, 6, 15);
   PWM_Def_Pattern(PWM5, 7, 16);
   PWM_Def_Pattern(PWM6, 8, 17);
   PWM_Def_Pattern(PWM7, 9, 18);*/

    /*********** SPI Port Configuration
    PC2/SPI2_MISO
    PC3/SPI2_MOSI
    PB10/SPI2_SCK
    ***********************************************/
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);

    GPIO_PinAFConfig(GPIOC,GPIO_PinSource2,GPIO_AF_SPI2);
    GPIO_PinAFConfig(GPIOC,GPIO_PinSource3,GPIO_AF_SPI2);
    GPIO_PinAFConfig(GPIOB,GPIO_PinSource10,GPIO_AF_SPI2);
    GPIO_PinAFConfig(GPIOB,GPIO_PinSource9,GPIO_AF_SPI2);

    GPIO_Pin_Config(GPIO_Pin_2,GPIO_Mode_AF,GPIOC);
    GPIO_Pin_Config(GPIO_Pin_3,GPIO_Mode_AF,GPIOC);
    GPIO_Pin_Config(GPIO_Pin_10,GPIO_Mode_AF,GPIOB);
    GPIO_Pin_Config(GPIO_Pin_9,GPIO_Mode_AF,GPIOB);

    SPI_Config();
    //SPI_CalculateCRC(SPI2,ENABLE);

    SPI_ITConfig(SPI2,SPI_I2S_IT_TXE,ENABLE);
    NVIC_Config(SPI2_IRQn,0,1,ENABLE);

    Configure_PD0();


    PWM_Init(PWMInit);

    while(1);

}


/*********************** Middleware layer code ***********************/
static void GPIO_Pin_Config(uint32_t GPIO_Pin_x,GPIOMode_TypeDef GPIO_Mode_x,GPIO_TypeDef* GPIOx)
{
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_x;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_x;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOx, &GPIO_InitStructure);
}

static void NVIC_Config(uint8_t x_IRQn,uint8_t Priority_No,uint8_t SubPrio_No,FunctionalState Enable_Or_Disable)
{
    nvicStructure.NVIC_IRQChannel = x_IRQn;
    nvicStructure.NVIC_IRQChannelPreemptionPriority = Priority_No;
    nvicStructure.NVIC_IRQChannelSubPriority = SubPrio_No;
    nvicStructure.NVIC_IRQChannelCmd = Enable_Or_Disable;
    NVIC_Init(&nvicStructure);
}

static void SPI_Config(void)
{
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_16b;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Hard;
    //SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;


    SPI_Init(SPI2,&SPI_InitStructure);

    SPI_SSOutputCmd(SPI2,ENABLE);

}

static void Configure_PD0(void) {
    /* Enable clock for GPIOD */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    /* Enable clock for SYSCFG */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    /* Set pin as input */
    #if 0
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOD, &GPIO_InitStruct);
    #endif

    GPIO_Pin_Config(GPIO_Pin_0,GPIO_Mode_IN,GPIOD);

    /* Tell system that you will use PD0 for EXTI_Line0 */
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOD, EXTI_PinSource0);

    /* PD0 is connected to EXTI_Line0 */
    EXTI_InitStruct.EXTI_Line = EXTI_Line0;
    /* Enable interrupt */
    EXTI_InitStruct.EXTI_LineCmd = ENABLE;
    /* Interrupt mode */
    EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
    /* Triggers on rising and falling edge */
    EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling;
    /* Add to EXTI */
    EXTI_Init(&EXTI_InitStruct);


    /* Add IRQ vector to NVIC */
    /* PD0 is connected to EXTI_Line0, which has EXTI0_IRQn vector */
    NVIC_Config(EXTI0_IRQn,0x00,0x00,ENABLE);
    #if 0
    NVIC_InitStruct.NVIC_IRQChannel = EXTI0_IRQn;
    /* Set priority */
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
    /* Set sub priority */
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
    /* Enable interrupt */
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    /* Add to NVIC */
    NVIC_Init(&NVIC_InitStruct);
    #endif
}

/* Set interrupt handlers */
/* Handle PD0 interrupt */
void EXTI0_IRQHandler(void) {
    /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line0) != RESET) {

        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line0);

        switch(Cmd_Type)
        {
        case PWMInit:
            PWM_Def(PWMDef);
            Cmd_Type = PWMDef;
        break;
        case PWMDef:

            i++;
            if(i != MAX_Size)
            {
                PWM_Def(PWMDef);
            }
            else
            {
                i = 0;
                Cmd_Type = PWMStart;
                PWM_Start(PWMStart);
            }
        break;
        case PWMStart:
            Cmd_Type = PWMStop;
            PWM_Stop(PWMStop);
            //Cmd_Type = PWMInit;
        break;
        case PWMStop:
            #if Continue_Exec
            Cmd_Type = PWMStart;
            PWM_Start(PWMStart);
            #else
            //Cmd_Type = PWMInit;
            #endif

            //PWM_Stop(PWMStop);
            //PWM_Init(PWMInit); if we want to repeat agin from PWMInit
            //Cmd_Type = PWMInit;
        break;
        }
    }
}


void PWM_Def_Pattern(u16 PWM_SigVal, u32 PWM_TimInst1, u32 PWM_TimInst2)
{
     if(PWM_TimInst1 == 0)
     {
         tcount ++;
     }
     else
     {
         PWM_Define_Par[instCount].PWM_Sig_Value = PWM_SigVal;
         //totalsig = totalsig | PWM_SigVal;
         PWM_Define_Par[instCount].PWM_Time_Instance_value = PWM_TimInst1;
         PWM_Define_Par[instCount].ONorOFF = 1;

         instCount ++;
         tcount ++;
     }

     if(PWM_TimInst2 == 0)
     {
         tcount ++;
     }
     else
     {
         //PWM_Define_Par[instCount].PWM_Sig_Value = totalsig & (~PWM_SigVal);
         PWM_Define_Par[instCount].PWM_Sig_Value = ~PWM_SigVal;
         PWM_Define_Par[instCount].PWM_Time_Instance_value = PWM_TimInst2;
         PWM_Define_Par[instCount].ONorOFF = 0;

         instCount ++;
         tcount ++;
     }

     if (tcount == Total_Time_Instances)
     {
         tcount = 0;
         instCount = 0;
     }

}


void sorting_PWM_Def(void)
{
   u32 i,j;
   u32 n = Total_Time_Instances;

   for (i = 1; i < Total_Time_Instances; i++)
      for (j = 0; j < Total_Time_Instances - i; j++) {
         if (PWM_Define_Par[j].PWM_Time_Instance_value > PWM_Define_Par[j+1].PWM_Time_Instance_value) {
            temp = PWM_Define_Par[j];
            PWM_Define_Par[j] = PWM_Define_Par[j + 1];
            PWM_Define_Par[j + 1] = temp;
         }
      }
}


void make_PWMPattern_Time_Arr(void)
{
    u32 l = 0;
    u32 j = 0;
    u32 valTimIns = 0;
    u32 current_value = 0;
    u32 previous_value = 0;
    u8 firstTime = SET;
    u8 current_OnorOff = 0;
    u16 totalsig =0;
    for (j = 0; j < Total_Time_Instances; j++)
    {
        current_value = PWM_Define_Par[j].PWM_Time_Instance_value;
        current_OnorOff = PWM_Define_Par[j].ONorOFF;
        if(previous_value == current_value)
        {

             if(current_OnorOff == 1)
             {
                 totalsig =   totalsig | PWM_Define_Par[j].PWM_Sig_Value;
                 PWMPattern_Time[l][0] = totalsig;

             }
             else
             {
                 totalsig = totalsig & PWM_Define_Par[j].PWM_Sig_Value;
                 PWMPattern_Time[l][0] = totalsig;
             }
             PWMPattern_Time[l][1] = current_value * TimerFrequency_MHz;
            //PWMPattern_Time[l][0] = PWMPattern_Time[l][0]  | PWM_Define_Par[j].PWM_Sig_Value;


        }
        else
        {
             if(firstTime == SET)
             {
                 firstTime = RESET;
             }
             else
             {
                 l++;
             }


             PWMPattern_Time[l][1] =   current_value * TimerFrequency_MHz;
             if(current_OnorOff == 1)
             {
                 totalsig =   totalsig | PWM_Define_Par[j].PWM_Sig_Value;
                 PWMPattern_Time[l][0] = totalsig;
             }
             else
             {
                 totalsig = totalsig & PWM_Define_Par[j].PWM_Sig_Value;
                 PWMPattern_Time[l][0] = totalsig;
             }
             valTimIns ++;

        }

        previous_value = current_value;
    }

    MAX_Size = valTimIns; /* MAX_SIZE is the total no of distinct time instances **/

}
