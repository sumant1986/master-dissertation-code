/**** Function declaration ****/

void PWM_Init(u8 InitCmd);
void PWM_Def(u8 DefCmd);
void PWM_Start(u8 StartCmd);
void PWM_Stop(u8 StopCmd);

void PWM_Def_Pattern(u16 PWM_SigVal, u32 PWM_TimInst1, u32 PWM_TimInst2);
void sorting_PWM_Def(void);
void make_PWMPattern_Time_Arr(void);

volatile u8 i = 0;

/****************************** Initial values of PWM Parameters ***************/
u8 Cmd_Type = PWMStop; //PWMInit;


/*** For PWMInit Command ***/
u16 InitPattern ; //= 0x0000; //PWM0 | PWM7| PWM15;

/*** For PWMDef Command ***/
u32 PWMPattern_Time[14][2] = {0};

/*u32 PWMPattern_Time[16][2] = {
                             {0x0000,1 * TimerFrequency_MHz},
                             {PWM0|PWM1|PWM2|PWM3,2 * TimerFrequency_MHz},
                             {0x0000,3 * TimerFrequency_MHz},
                             {PWM12|PWM13|PWM14|PWM15,4 * TimerFrequency_MHz},

                             {0x0000,5 * TimerFrequency_MHz},
                             {PWM1|PWM3|PWM5|PWM7,6 * TimerFrequency_MHz},
                             {0x0000,7 * TimerFrequency_MHz},
                             {PWM9|PWM11|PWM13|PWM15,8 * TimerFrequency_MHz},

                             {0x0000,9 * TimerFrequency_MHz},
                             {0x0000,10 * TimerFrequency_MHz},
                             {0x0000,11 * TimerFrequency_MHz},
                             {0x0000,12 * TimerFrequency_MHz},

                             {0x0000,13 * TimerFrequency_MHz},
                             {PWM0|PWM2|PWM4|PWM6,14 * TimerFrequency_MHz},
                             {0x0000,15 * TimerFrequency_MHz},
                             {PWM8|PWM10|PWM12|PWM14,16 * TimerFrequency_MHz}

                           };*/

/*** For PWMStart Command ***/
u32 No_Of_Repetition =  0; //0x00055555;
u32 TimePeriod = 10000 * TimerFrequency_MHz; /* 10ms */
/********************************************************************************************/
