#include "main.h"
#include "stm32f4xx_tim.h"
#include "command.h"
#include "function_api_interface.h"


static void GPIO_Pin_Config(uint32_t GPIO_Pin_x,GPIOMode_TypeDef GPIO_Mode_x,GPIO_TypeDef* GPIOx);
static void Timer_Config(uint16_t Prescaler_Value,uint32_t Period_Value,uint16_t Counter_Mode,TIM_TypeDef* TIMx);
static void NVIC_Config(uint8_t x_IRQn,uint8_t Priority_No,uint8_t SubPrio_No,FunctionalState Enable_Or_Disable);
static void SPI_Config(void);
static void Configure_PD1(void);

GPIO_InitTypeDef  GPIO_InitStructure;
TIM_TimeBaseInitTypeDef timerInitStructure;
NVIC_InitTypeDef nvicStructure;

SPI_InitTypeDef SPI_InitStructure;

//GPIO_InitTypeDef GPIO_InitStruct;
EXTI_InitTypeDef EXTI_InitStruct;
//NVIC_InitTypeDef NVIC_InitStruct;

static u8 Toggle_Index = 0;
static u32 ReptCount = 0;
static u32 Count = 0;
static u32 TimPeriod = 0;
static u16 InitPtrn = 0;

static u8 Emergency_Stop = RESET;

/********************************* User define ************************************/
#define MAX_Size 14  /* MAX_SIZE is the total no of distinct time instances.
                        Its value should be same as defined in Master controller  **/
/**********************************************************************************/
struct PWM
{
u32 CCR_value;
u16 Signal_no;
}PWM_Signal[MAX_Size];

u32 ab1,ab2,ab3,ab4,ab5,ab6,ab7,ab8,ab9,ab10 = 0;
/************************************************************************************
 *  File:     main.c
 *  Purpose:  Cortex-M4 main file.
 *            Replace with your code.
 *  Date:     05 July 2013
 *  Info:     If __NO_SYSTEM_INIT is defined in the Build options,
 *            the startup code will not branch to SystemInit()
 *            and the function can be removed
 ************************************************************************************/


#ifndef __NO_SYSTEM_INIT
//void SystemInit()
//{}
#endif
//int getValue = 0;
//int k= 0;
uint8_t x= 0;
/*********************************************************************
 *
 *  main()
 *********************************************************************/
void main()
{
  /******************************************************************
   *
   *  Place your code here.
   ******************************************************************/
    /*********** SPI Port Configuration
    PC2/SPI2_MISO
    PC3/SPI2_MOSI
    PB10/SPI2_SCK
    ***********************************************/
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);

    GPIO_PinAFConfig(GPIOC,GPIO_PinSource2,GPIO_AF_SPI2);
    GPIO_PinAFConfig(GPIOC,GPIO_PinSource3,GPIO_AF_SPI2);
    GPIO_PinAFConfig(GPIOB,GPIO_PinSource10,GPIO_AF_SPI2);
    GPIO_PinAFConfig(GPIOB,GPIO_PinSource9,GPIO_AF_SPI2);

    GPIO_Pin_Config(GPIO_Pin_2,GPIO_Mode_AF,GPIOC);
    GPIO_Pin_Config(GPIO_Pin_3,GPIO_Mode_AF,GPIOC);
    GPIO_Pin_Config(GPIO_Pin_10,GPIO_Mode_AF,GPIOB);
    GPIO_Pin_Config(GPIO_Pin_9,GPIO_Mode_AF,GPIOB);

    SPI_Config();
    //SPI_CalculateCRC(SPI2,ENABLE);

    SPI_ITConfig(SPI2,SPI_I2S_IT_RXNE,ENABLE);
    NVIC_Config(SPI2_IRQn,0,1,ENABLE);

    SPI_Cmd(SPI2,ENABLE);

    /* GPIOF Periph clock enable */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);
    GPIO_Pin_Config(GPIO_Pin_All,GPIO_Mode_OUT,GPIOF);

    /* GPIOD Periph clock enable */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    GPIO_Pin_Config(GPIO_Pin_0,GPIO_Mode_OUT,GPIOD);

    /* TIM2 peripheral enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	/* TIM5 peripheral enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);

	/* TIM2 Interrupt Configure */
	TIM_ITConfig(TIM2, TIM_IT_CC1, ENABLE);

	/* TIM5 Interrupt Configure */
    TIM_ITConfig(TIM5, TIM_IT_Update, ENABLE);

    /* NVIC Configure */
    NVIC_Config(TIM2_IRQn,0,1,ENABLE);
    NVIC_Config(TIM5_IRQn,0,1,ENABLE);

    Configure_PD1();

  while(1)
  {
    ;
  }

}


void TIM2_IRQHandler(void)
{

    if (TIM2->SR & TIM_IT_CC1 != RESET)
    {
        //Clear the interrupt pending flag for timer update
        TIM2->SR = (uint16_t)~TIM_IT_CC1;

        GPIOF->ODR = (PWM_Signal[x].Signal_no);
        x = x + 1;

        if(x < MAX_Size)
        {
	        ;
        }
        else
        {
            x = 0;
        }

        TIM2->CCR1 = PWM_Signal[x].CCR_value;


    }
    else
    {
        ;
    }
}

void TIM5_IRQHandler(void)
{
    if(TIM5->SR & TIM_IT_Update != RESET)
    {
       TIM5->SR = (uint16_t)~TIM_IT_Update;
       GPIOF->ODR = InitPtrn;
       Count ++;

       if(ReptCount == 0)
       {
           ;
       }
       else if(Count == ReptCount)
       {
            TIM_Cmd(TIM2, DISABLE);
            TIM_Cmd(TIM5, DISABLE);

            TIM2->CNT = 0;
            TIM5->CNT = 0;
            Count = 0;
            Emergency_Stop = RESET;
            NVIC_Config(EXTI1_IRQn,0x00,0x00,DISABLE);
            /* Pinlow for sending next command*/
            GPIOD->BSRRH = GPIO_Pin_0 ;

       }
       else
       {
         ;
       }
    }

    else
    {
        ;
    }

}

void Initialize_PWM(void)
{
  InitPtrn   = (u16)(RecvBuffer[0]);
  GPIOF->ODR = InitPtrn;

  /* Pinlow for sending next command*/
  GPIOD->BSRRH = GPIO_Pin_0 ;
}

void Define_PWM(void)
{

    PWM_Signal[Toggle_Index].Signal_no = (u16)(RecvBuffer[0]);
    PWM_Signal[Toggle_Index].CCR_value = (RecvBuffer[1]) | (RecvBuffer[2] << 16u);
    Toggle_Index ++ ;

    if(Toggle_Index == MAX_Size)
    {
        Toggle_Index = 0;
    }
    /* Pinlow for sending next command*/
    GPIOD->BSRRH = GPIO_Pin_0 ;
}

void Start_PWM(void)
{
    ReptCount = (RecvBuffer[0]) | (RecvBuffer[1] << 16u);
    TimPeriod = (RecvBuffer[2]) | (RecvBuffer[3] << 16u);

    Timer_Config(1,TimPeriod,TIM_CounterMode_Up,TIM2);
    Timer_Config(1,TimPeriod,TIM_CounterMode_Up,TIM5);
    x = 0;
    TIM_SetCompare1(TIM2,PWM_Signal[x].CCR_value);

    NVIC_Config(EXTI1_IRQn,0x00,0x00,ENABLE);

    /* TIM2 Enable */
    TIM_Cmd(TIM2, ENABLE);
    /* TIM5 Enable */
    TIM_Cmd(TIM5, ENABLE);

}

/*********************** Middleware layer code ***********************/
static void GPIO_Pin_Config(uint32_t GPIO_Pin_x,GPIOMode_TypeDef GPIO_Mode_x,GPIO_TypeDef* GPIOx)
{
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_x;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_x;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOx, &GPIO_InitStructure);
}

static void Timer_Config(uint16_t Prescaler_Value,uint32_t Period_Value,uint16_t Counter_Mode,TIM_TypeDef* TIMx)
{
    timerInitStructure.TIM_Prescaler = Prescaler_Value-1;
    timerInitStructure.TIM_CounterMode = Counter_Mode;
    timerInitStructure.TIM_Period = Period_Value;
    timerInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    timerInitStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIMx, &timerInitStructure);
}

static void NVIC_Config(uint8_t x_IRQn,uint8_t Priority_No,uint8_t SubPrio_No,FunctionalState Enable_Or_Disable)
{
    nvicStructure.NVIC_IRQChannel = x_IRQn;
    nvicStructure.NVIC_IRQChannelPreemptionPriority = Priority_No;
    nvicStructure.NVIC_IRQChannelSubPriority = SubPrio_No;
    nvicStructure.NVIC_IRQChannelCmd = Enable_Or_Disable;
    NVIC_Init(&nvicStructure);
}


static void SPI_Config(void)
{
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_16b;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Hard;
    SPI_InitStructure.SPI_Mode = SPI_Mode_Slave;
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_RxOnly;
    SPI_Init(SPI2,&SPI_InitStructure);
}

static void Configure_PD1(void) {
    /* Enable clock for GPIOD */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    /* Enable clock for SYSCFG */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    /* Set pin as input */
    GPIO_Pin_Config(GPIO_Pin_1,GPIO_Mode_IN,GPIOD);

    /* Tell system that you will use PD1 for EXTI_Line1 */
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOD, EXTI_PinSource1);

    /* PD0 is connected to EXTI_Line1 */
    EXTI_InitStruct.EXTI_Line = EXTI_Line1;
    /* Enable interrupt */
    EXTI_InitStruct.EXTI_LineCmd = ENABLE;
    /* Interrupt mode */
    EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
    /* Triggers on rising and falling edge */
    EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
    /* Add to EXTI */
    EXTI_Init(&EXTI_InitStruct);

    /* Add IRQ vector to NVIC */
    /* PD2 is connected to EXTI_Line1, which has EXTI1_IRQn vector */

    NVIC_Config(EXTI1_IRQn,0x00,0x00,ENABLE);

}


void EXTI1_IRQHandler(void) {
    /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line1) != RESET) {

        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line1);


        if(Emergency_Stop == RESET)
        {

            TIM_Cmd(TIM2, DISABLE);
            TIM_Cmd(TIM5, DISABLE);

            GPIOF->ODR = InitPtrn ;
            Emergency_Stop = SET;
        }
        else if(Emergency_Stop == SET)
        {

            TIM_Cmd(TIM2, ENABLE);
            TIM_Cmd(TIM5, ENABLE);

            Emergency_Stop = RESET;
        }
        else
        {
            ;
        }

    }
}
