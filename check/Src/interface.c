#include "stm32f4xx.h"
#include "command.h"
#include "function_api.h"

static u8 Init_Cmd_Send_Status = 0;
static u8 Init_Pattern_Send_Status = 0;

static u8 Def_Cmd_Send_Status = 0;
static u8 Def_Pattern_Send_Status = 0;
static u8 Def_TimeInst_Send_Status = 0;

static u8 Start_Cmd_Send_Status = 0;
static u8 Start_Rept_Send_Status = 0;
static u8 FirstFrame_Status = 0;
static u8 SecondFrame_Status = 0;
static u16 Send_Frame = 0;

static u8 Stop_Cmd_Send_Status = 0;

static u8 j =0;
static u8 FirstTime = SET;
static u8 BufSize = 0;
static u8 InvalidCmd = RESET;
static u16 RecvCmd = 0;

u32 z =0;
#define ContinueExec 0

/********************************* Function Declaration *******************/



/********************************** Function Definition *******************/
#if 0
void PWM_Init(u8 InitCmd)
{
    Cmd_Type = InitCmd;

    SPI_I2S_SendData(SPI2,InitCmd);
    Init_Cmd_Send_Status = 1;
    SPI_Cmd(SPI2,ENABLE);
}

void PWM_Def(u8 DefCmd)
{
    SPI_I2S_SendData(SPI2,DefCmd);
    Def_Cmd_Send_Status = 1;
    SPI_Cmd(SPI2,ENABLE);
}

void PWM_Start(u8 StartCmd)
{
    SPI_I2S_SendData(SPI2,StartCmd);
    Start_Cmd_Send_Status = 1;
    SPI_Cmd(SPI2,ENABLE);
}

void PWM_Stop(u8 StopCmd)
{
    SPI_I2S_SendData(SPI2,StopCmd);
    Stop_Cmd_Send_Status = 1;
    SPI_Cmd(SPI2,ENABLE);

}
#endif

void SPI2_IRQHandler(void)
{
    if(SPI_I2S_GetFlagStatus(SPI2, SPI_FLAG_RXNE) != RESET)
    {
         if(FirstTime == SET)
         {
             RecvCmd = SPI2->DR;
             FirstTime = RESET;
             GPIOD->BSRRL = GPIO_Pin_0 ;
             switch(RecvCmd)
              {
                  case PWMInit:
                      BufSize = 1;
                  break;

                  case PWMDef:
                      BufSize = 3;
                  break;

                  case PWMStart:
                      BufSize = 4;
                  break;

                  case PWMStop:
                      FirstTime = SET;
                      j = 0;
                      GPIOD->BSRRH = GPIO_Pin_0 ;
                      #if ContinueExec
                      #else
                      //SPI_Cmd(SPI2,DISABLE);
                      #endif
                  break;

                  default:
                      InvalidCmd = SET;
                  break;
              }

         }
         else
         {
              RecvBuffer[j++] = SPI2->DR;
              if(j >= BufSize)
              {
                  FirstTime = SET;
                  j = 0;
                  //z = SPI_GetCRC(SPI2,SPI_CRC_Rx);
                  if(InvalidCmd == RESET)
                  {
                       switch(RecvCmd)
                       {
                           case PWMInit:
                                Initialize_PWM();
                           break;
                           case PWMDef:
                                Define_PWM();
                           break;
                           case PWMStart:
                                Start_PWM();
                           break;
                           case PWMStop:
                           break;
                           default:
                           break;
                         }
                      }
                  }
         }

    }

}

