/**** Function declaration ************/
void PWM_Init(u8 InitCmd);
void PWM_Def(u8 DefCmd);
void PWM_Start(u8 StartCmd);
void PWM_Stop(u8 StopCmd);

void Initialize_PWM(void);
void Define_PWM(void);
void Start_PWM(void);

volatile u8 i = 0;
u8 Cmd_Type = PWMStop; //PWMInit;

#if 0
/*** For PWMInit Command ***/
u16 InitPattern = 0x5555;

/*** For PWMDef Command ***/

u32 PWMPattern_Time[][2] = {
                             {0x0000,12.9 * TimerFrequency_MHz},
                             {0x0000,12.9 * TimerFrequency_MHz},
                             {0x0000,12.9 * TimerFrequency_MHz}

                          };

/*** For PWMStart Command ***/
u32 No_Of_Repetition = 0;
u32 TimePeriod = 0;
#endif

u32 RecvBuffer[4] = {0};

