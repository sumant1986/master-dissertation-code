
extern void PWM_Init(u8 InitCmd);
extern void PWM_Def(u8 DefCmd);
extern void PWM_Start(u8 StartCmd);
extern void PWM_Stop(u8 StopCmd);

extern void Initialize_PWM(void);
extern void Define_PWM(void);
extern void Start_PWM(void);

extern volatile u8 i;
extern u8 Cmd_Type ;

#if 0
extern u16 InitPattern;

extern u16 Pattern[32] ;
extern u32 TimeInstance[32] ;


extern u32 No_Of_Repetition;
extern u32 TimePeriod ;
#endif

extern u32 RecvBuffer[4];
